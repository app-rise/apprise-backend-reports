package test.apprise.backend.analytics;

import static apprise.backend.analytics.api.ApiConstants.viewapi;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.core.GenericType;

import org.junit.Test;

import apprise.backend.analytics.ViewDescriptor;
import apprise.backend.analytics.ViewUrls;
import apprise.backend.analytics.ViewDescriptor.Type;
import apprise.backend.analytics.api.Dtos.ViewAndDescriptorAndParams;
import apprise.backend.analytics.api.Dtos.ViewParams;
import apprise.backend.analytics.metabase.Dtos;
import apprise.backend.analytics.metabase.TokenGenerator;
import lombok.Data;
import test.apprise.backend.Testbase;

public class ViewApiTest extends Testbase {

    @Inject
    TokenGenerator generator;

    @Inject
    MockMetabase mock;

    @Data
    public static class AllViewHandler {

        List<ViewDescriptor> views;

        @GET
        public Object get() {

            return "[{\"type\":\"dashboard\", \"name\":\"test name\", \"id\":1}]";
        }

    }

    @Data
    public static class RenderHandler {

        String result;

        @GET
        public Object get() {

            return result;

        }

    }

    @Test
    public void can_list_and_render_views() {

        var view = new ViewDescriptor().type(Type.question).id("1").name("test name");

        mock.handler(new AllViewHandler().views(List.of(view)));

        var views = at(viewapi).get(collofdescriptors);

        assertTrue(views.contains(view));

        var params = new ViewParams().plain(Map.of("some    test","testval"));

        var dto = new ViewAndDescriptorAndParams().view(view).params(params);

        var url = at(viewapi).post( jsonOf (dto)).readEntity(ViewUrls.class);

        var token = new Dtos.Token().question(view.id());

        assertTrue(url.embedUrl().contains(generator.generate(token)));

    }

    static GenericType<List<ViewDescriptor>> collofdescriptors = new GenericType<List<ViewDescriptor>>() {
    };

}
