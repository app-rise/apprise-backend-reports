package test.apprise.backend.analytics;


import static test.apprise.backend.Testmate.all;
import static test.apprise.backend.Testmate.listOf;

import java.util.List;

import javax.enterprise.context.Dependent;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import apprise.backend.analytics.Dataview;
import apprise.backend.analytics.data.DataviewDao;
import apprise.backend.analytics.data.DataviewService;
import apprise.backend.data.Events.DatabaseReady;
import lombok.SneakyThrows;

@Dependent
public class Testmate {

    private static Testmate _this;

    static void initSuite(@Observes DatabaseReady event, Testmate mate) {

        _this = mate;
    }

    @Inject
    DataviewDao dao;

    @Inject
    DataviewService service;


    @SneakyThrows
    public static Dataview stage(Dataview sub) {

        if (sub.id() != null && _this.dao.lookup(sub.id()).isPresent())
            return sub;


        return _this.service.add(sub);

    }

    public static List<Dataview> stage(Dataview... dataviews) {

        return listOf(all(dataviews).map(Testmate::stage));

    }
}