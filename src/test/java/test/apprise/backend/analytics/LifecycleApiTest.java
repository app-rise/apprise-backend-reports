package test.apprise.backend.analytics;

import static apprise.backend.analytics.api.ApiConstants.dataviewapi;
import static java.net.HttpURLConnection.HTTP_NOT_MODIFIED;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static javax.ws.rs.core.HttpHeaders.IF_NONE_MATCH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.analytics.Fixture.someDataview;
import static test.apprise.backend.analytics.Testmate.stage;
import static test.apprise.backend.api.Apimate.assertFailed;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;
import static test.apprise.backend.api.Apimate.jsonOf;

import java.util.List;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import apprise.backend.analytics.Dataview;
import lombok.SneakyThrows;
import test.apprise.backend.Testbase;

public class LifecycleApiTest extends Testbase {

    @Test
    @SneakyThrows
    public void dataviews_can_be_added_and_fetched() {

        var dataview = someDataview();

        var response = at(dataviewapi).post(jsonOf(dataview));

        assertSuccessful(response);

        Dataview added = response.readEntity(Dataview.class);

        var all = at(dataviewapi).get(collofdataviews);

        assertTrue(all.contains(added));

        var lookup = at(dataviewapi, added.id()).get(Dataview.class);

        assertEquals(added, lookup);

    }

    @Test
    public void dataviews_can_be_fetched_in_bulk_and_cached() {

        var first = stage(someDataview());

        Response response = at(dataviewapi).get();

        var etag = response.getEntityTag();

        response = at(dataviewapi).header(IF_NONE_MATCH, etag).get();

        // cache is hit
        assertEquals(HTTP_NOT_MODIFIED, response.getStatus());

        // make some change.
        stage(someDataview());

        response = at(dataviewapi).header(IF_NONE_MATCH, etag).get();

        // cache is invalidated
        assertNotEquals(HTTP_NOT_MODIFIED, response.getStatus());

        etag = response.getEntityTag();

        response = at(dataviewapi).header(IF_NONE_MATCH, etag).get();

        // cache is hit again.
        assertEquals(HTTP_NOT_MODIFIED, response.getStatus());

        // remove older dataview
        response = at(dataviewapi, first.id()).delete();

        response = at(dataviewapi).header(IF_NONE_MATCH, etag).get();

        // cache is invalidated again.
        assertNotEquals(HTTP_NOT_MODIFIED, response.getStatus());
    }

    @Test
    public void dataviews_can_be_udpated() {

        var dataview = stage(someDataview());

        dataview.lifecycle().touchedBy("somebody");

        Response response = at(dataviewapi, dataview.id()).put(jsonOf(dataview));

        assertSuccessful(response);

        var updated = response.readEntity(Dataview.class);

        assertEquals(dataview, updated);

        var fetched = at(dataviewapi, dataview.id()).get(Dataview.class);

        assertEquals(dataview, fetched);

    }

    @Test
    public void dataviews_can_be_deleted() {

        var dataview = stage(someDataview());

        Response response = at(dataviewapi, dataview.id()).delete();

        assertEquals(HTTP_NO_CONTENT, response.getStatus());

        response = at(dataviewapi, dataview.id()).get();

        assertFailed(response);

    }

    static GenericType<List<Dataview>> collofdataviews = new GenericType<List<Dataview>>() {
    };
}
