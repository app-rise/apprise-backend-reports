package test.apprise.backend.analytics;

import static apprise.backend.analytics.Constants.dataviewPrefix;

import apprise.backend.analytics.Dataview;
import apprise.backend.analytics.ViewDescriptor;
import test.apprise.backend.Testmate;

public class Fixture extends Testmate {

    public static Dataview someDataview() {

        return new Dataview().id(any(dataviewPrefix)).views(listOf(anyStream( i -> someView(), 3)));

    }

    public static ViewDescriptor someView() {

        return new ViewDescriptor().id(any("V")).name(any("view"));
    }

}
