package test.apprise.backend.analytics;

import java.util.Map;

import javax.inject.Inject;

import org.junit.Test;

import apprise.backend.analytics.metabase.Dtos;
import apprise.backend.analytics.metabase.TokenGenerator;
import test.apprise.backend.Testbase;

public class TokenLab extends Testbase {
    
    @Inject
    TokenGenerator generator;

    @Test
    public void generate_token() {

        var token = new Dtos.Token().dashboard("2").params(Map.of("1","LVA"));

        show(generator.generate(token));

    }
}
