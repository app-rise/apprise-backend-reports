package test.apprise.backend.analytics;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import test.apprise.backend.Events.TestLifecycle.EndTest;

@ApplicationScoped
@Path("mock")
@Slf4j
public class MockMetabase {

    public static class Handler {

        @GET
        @Produces(APPLICATION_JSON)
        public Response get() {

            return Response.status(Status.INTERNAL_SERVER_ERROR).entity("{\"error\":\"not mocked!\"}").build();
        }
    }

    @Setter
    private Object handler = new Handler();

    @Path("{path: .*}")
    public Object resource() {
    
        return handler;
    }

    void resetOnTestEnd(@Observes EndTest event) {

        log.trace(("resetting mock metabase handler..."));
        
        handler = new Handler();
    }

}
