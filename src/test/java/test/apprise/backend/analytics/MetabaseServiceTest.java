package test.apprise.backend.analytics;

import static apprise.backend.analytics.api.ApiConstants.viewapi;
import static apprise.backend.analytics.metabase.Constants.apikey_header;
import static org.junit.Assert.assertTrue;
import static test.apprise.backend.api.Apimate.assertSuccessful;
import static test.apprise.backend.api.Apimate.at;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.junit.Test;

import test.apprise.backend.Testbase;

public class MetabaseServiceTest extends Testbase {

    @Inject
    MockMetabase mock;

    public class ProbeHandler {

        @GET
        public Response get(@Context HttpHeaders headers, @Context UriInfo info) {

            assertTrue(headers.getRequestHeaders().containsKey(apikey_header));

            return Response.ok(List.of()).build();
        }

    }

    @Test
    public void calls_carry_a_key() {

        mock.handler(new ProbeHandler());

        var response = at(viewapi).get();

        assertSuccessful(response);

        assertTrue(response.readEntity(List.class).isEmpty());

    }


}
