package apprise.backend.analytics;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import lombok.Data;

@Data
public class ViewDescriptor {

    public enum Type { dashboard, question }
    
    Type type = Type.dashboard;

    String id;
    String name;

    @JsonAnyGetter
    @JsonAnySetter
    Map<String,Object> details = new HashMap<>();

}
