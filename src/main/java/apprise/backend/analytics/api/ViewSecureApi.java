package apprise.backend.analytics.api;
import static java.lang.Integer.MAX_VALUE;

import java.util.List;

import javax.annotation.Priority;
import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

import apprise.backend.analytics.ViewDescriptor;
import apprise.backend.analytics.ViewUrls;
import apprise.backend.analytics.api.Dtos.ViewAndDescriptorAndParams;

@Decorator @Priority(MAX_VALUE)
public class ViewSecureApi implements ViewApi {

    @Inject @Delegate
	ViewApi unsecure;
	
	// @Inject
	// User user;
	
	@Override
	public List<ViewDescriptor> all() {
        return unsecure.all();
	}

	@Override
	public ViewUrls resolve(ViewAndDescriptorAndParams view) {
		return unsecure.resolve(view);
	}
    
}
