package apprise.backend.analytics.api;

public class ApiConstants {

    public static final String dataviewapi = "analytics";
    public static final String viewapi = dataviewapi+"/view";

}
