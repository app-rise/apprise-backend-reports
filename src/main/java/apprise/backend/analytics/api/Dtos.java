package apprise.backend.analytics.api;

import java.util.Map;

import apprise.backend.analytics.ViewDescriptor;
import lombok.Data;

public class Dtos {

    @Data
    public static class ViewParams {

        public static final ViewParams empty = new ViewParams();

        Map<String, String> plain = Map.of();
        Map<String, String> encoded = Map.of();

    }

    @Data
    public static class ViewAndDescriptorAndParams {

        ViewDescriptor view;
        ViewParams params;

    }
}
