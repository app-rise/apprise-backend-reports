package apprise.backend.analytics.api;

import static apprise.backend.analytics.api.ApiConstants.viewapi;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import apprise.backend.analytics.ViewDescriptor;
import apprise.backend.analytics.ViewUrls;
import apprise.backend.analytics.api.Dtos.ViewAndDescriptorAndParams;
import apprise.backend.analytics.api.Dtos.ViewParams;
import apprise.backend.analytics.data.ViewService;

public interface ViewApi {

    List<ViewDescriptor> all();

    ViewUrls resolve(ViewAndDescriptorAndParams view);

    @ApplicationScoped
    @Path(viewapi)
    public static class Default implements ViewApi {

        @Context
        UriInfo info;

        @Context
        Request request;

        @Inject
        ViewService service;

        @GET
        @Produces(MediaType.APPLICATION_JSON)
        @Override
        public List<ViewDescriptor> all() {

            return service.all();
        }

        @POST
        @Produces(MediaType.APPLICATION_JSON)
        @Override
        public ViewUrls resolve(ViewAndDescriptorAndParams view) {

            return service.urlsFor(view.view(), view.params() == null ? ViewParams.empty : view.params());
        }
    }
}
