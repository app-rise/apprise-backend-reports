package apprise.backend.analytics.api;

import static java.util.Collections.emptyList;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonValue;

import apprise.backend.analytics.Dataview;
import apprise.backend.analytics.data.DataviewDao;
import apprise.backend.analytics.data.DataviewService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

public interface LifecycleApi {

    Response fetchAll(@Context Request request);

    Dataview fetchOne(String rid);

    Dataview addOne(Dataview dataview);

    Dataview updateOne(String rid, Dataview dataview);

    void removeOne(String rid);

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class Raw {

        @JsonRawValue
        @JsonValue // returns as string and without any object wrapping.
        List<String> data;
    }

    @ApplicationScoped
    @Path(ApiConstants.dataviewapi)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public static class Default implements LifecycleApi {

        @Inject
        DataviewDao dao;

        @Inject
        DataviewService service;

        @GET
        @Override
        public Response fetchAll(@Context Request request) {

            var timestamp = dao.changestamp();

            if (timestamp == null)
                return Response.ok(emptyList()).build();

            // this is a 'weak' etag (resources should be cached even if they're ont byte-equal).
            // without it, cache would be prevented between a compressed and uncompressed response.
            // a gateway like Ambassador would in fact be likely to drop a strong etag altogether for compressed responses.
            // weak etags make it through and capture the notion of logical change we're after.
            var etag = new EntityTag(String.valueOf(timestamp), true);

            var response = request.evaluatePreconditions(etag);

            if (response == null)
                return Response.ok(new Raw(dao.allRaw())).tag(etag).build();

            return response.build(); // returns 304 (Not Modified)
        }

        @GET
        @Override
        @Path("{rid}")
        public Dataview fetchOne(@NonNull @PathParam("rid") String sid) {

            return dao.get(sid);
        }

        @POST
        public Dataview addOne(@NonNull Dataview dataview) {

            return service.add(dataview);

        }

        @Override
        @PUT
        @Path("{rid}")
        public Dataview updateOne(@NonNull @PathParam("rid") String sid, Dataview dataview) {

            if (!dataview.id().equals(sid))
                throw new IllegalArgumentException(
                        String.format("dataview '%s' does not match payload ('%s')", sid, dataview.id()));

            return service.update(dataview);
        }

        @Override
        @DELETE
        @Path("{rid}")
        public void removeOne(@NonNull @PathParam("rid") String id) {

            service.remove(id);
        }
    }
}
