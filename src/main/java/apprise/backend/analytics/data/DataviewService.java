package apprise.backend.analytics.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import apprise.backend.analytics.Constants;
import apprise.backend.analytics.Dataview;
import apprise.backend.analytics.Dataview.DataviewState;
import apprise.backend.iam.model.User;
import apprise.backend.model.Id;
import apprise.backend.model.LifecycleQualifiers.Added;
import apprise.backend.model.LifecycleQualifiers.Removed;
import apprise.backend.model.LifecycleQualifiers.Updated;
import apprise.backend.transaction.Transactional;
import apprise.backend.validation.Mode;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Transactional
@Slf4j
public class DataviewService {

    @Inject
    DataviewDao dao;

    @Inject
    User logged;

    @Inject
    Event<Dataview> events;

    public Dataview add(@NonNull Dataview dataview) {

        if (dataview.id() == null)
            dataview.id(Id.mint(Constants.dataviewPrefix));

        if (dataview.lifecycle() == null)
            dataview.state(DataviewState.active);

        dataview.validateNow(Mode.create);

        dataview.lifecycle().touchedBy(logged.username());

        log.info("adding dataview {}", dataview.id());

        events.select(Added.beforeEvent).fire(dataview);

        dao.add(dataview);

        events.select(Added.event).fire(dataview);

        return dataview;

    }

    public Dataview update(Dataview updated) {

        Dataview dataview = dao.get(updated.id());

        dataview.updateWith(updated);

        dataview.validateNow(Mode.update);

        dataview.touchedBy(logged.username());

        log.info("updating dataview {}", dataview.id());

        events.select(Updated.beforeEvent).fire(dataview);

        dao.update(dataview);

        events.select(Updated.event).fire(dataview);

        return dataview;

    }

    public Dataview remove(@NonNull String id) {

        Dataview dataview = dao.get(id);

        dataview.validateNow(Mode.delete);

        // observers might still want to see who's removing this.
        dataview.touchedBy(logged.username());

        log.info("removing dataview {}", dataview.id());

         events.select(Removed.beforeEvent).fire(dataview);

        dao.remove(dataview);

        events.select(Removed.event).fire(dataview);

        return dataview;
    }
}
