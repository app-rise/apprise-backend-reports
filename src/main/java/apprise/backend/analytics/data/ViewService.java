package apprise.backend.analytics.data;

import java.util.List;

import apprise.backend.analytics.ViewDescriptor;
import apprise.backend.analytics.ViewUrls;
import apprise.backend.analytics.api.Dtos.ViewParams;

public interface ViewService {
    
    
    List<ViewDescriptor> all();

    ViewUrls urlsFor(ViewDescriptor descriptor, ViewParams params);
}
