package apprise.backend.analytics.data;

import static javax.interceptor.Interceptor.Priority.LIBRARY_BEFORE;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;

import apprise.backend.data.Events.DatabaseReady;
import lombok.extern.slf4j.Slf4j;

@ApplicationScoped
@Slf4j
public class Observers {

  // --------------------------------------- db staging

  public void stageTables(@Observes @Priority(LIBRARY_BEFORE) DatabaseReady event, DataviewSchema schema) {

    log.info("staging analytics tables (if required)");

    schema.stageTables();

  }


}
