package apprise.backend.analytics.data;

import static java.lang.String.format;
import static org.jooq.impl.DSL.name;
import static org.jooq.impl.DSL.table;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.RecordMapper;
import org.jooq.Table;

import apprise.backend.analytics.Dataview;
import apprise.backend.data.Config;
import apprise.backend.ser.Binder;
import lombok.Getter;

@ApplicationScoped
public class DataviewSchema {

        @Inject
        Config config;

        @Inject
        DSLContext jooq;

        @Getter
        Table<Record> dataviews;

        @Inject
        @Getter
        Binder<Dataview> binder;

        @Getter
        RecordMapper<Record, Dataview> mapper = record -> binder.bind(record.get(DataConstants.dataview_details).data());

        // no data binding at all.
        @Getter
        RecordMapper<Record, String> rawmapper = record -> record.get(DataConstants.dataview_details).data();

        public void stageTables() {

                dataviews = table(name(config.dbschema(), DataConstants.dataview_table));

                String table = format(

                                "create table if not exists %s ("
                                                + " %s varchar(100) generated always as (%s ->> '%s') stored primary key" // id
                                                + ", %s bigint generated always as (( coalesce(%s -> 'lifecycle' ->> 'lastModified', %s -> 'lifecycle' ->> 'created'))::bigint) stored not null" // timestamp
                                                + ", %s jsonb not null" // details

                                                + ");",
                                dataviews, // table
                                DataConstants.dataview_id.getName(), DataConstants.dataview_details.getName(),
                                DataConstants.dataview_id.getName(), // id
                                DataConstants.dataview_timestamp.getName(), DataConstants.dataview_details.getName(),
                                DataConstants.dataview_details.getName(), // timestamp
                                DataConstants.dataview_details.getName() // details            
                );

                //System.out.println(table);

                jooq.execute(table);

        }
}
