package apprise.backend.analytics.data;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.name;

import org.jooq.Field;
import org.jooq.JSONB;

public class DataConstants {

    public static final String dataview_table="analytics";
	
	public static final Field<String> dataview_id = field(name(dataview_table, "id"), String.class);
	public static final Field<Long>  dataview_timestamp = field(name(dataview_table, "timestamp"), Long.class);
	public static final Field<JSONB> dataview_details = field(name(dataview_table, "details"), JSONB.class);

	
}
