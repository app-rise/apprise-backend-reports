package apprise.backend.analytics.data;

import static apprise.backend.data.ErrorCode.duplicate;
import static apprise.backend.data.ErrorCode.map;
import static apprise.backend.data.ErrorCode.on;
import static java.lang.String.format;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jooq.DSLContext;
import org.jooq.InsertOnDuplicateStep;
import org.jooq.JSONB;
import org.jooq.impl.DSL;

import apprise.backend.Exceptions.NoSuchEntityException;
import apprise.backend.analytics.Dataview;

@ApplicationScoped
public class DataviewDao {

    @Inject
    DataviewSchema schema;

    @Inject
    DSLContext jooq;

    public List<Dataview> all() {
        return all(DataviewFilter.dataviews());
    }

    public List<Dataview> all(DataviewFilter filter) {

        return jooq.select().from(schema.dataviews())
                .where(filter.toCondition())
                .fetch(schema.mapper());
    }

    public List<String> allRaw() {
        return allRaw(DataviewFilter.dataviews());
    }

    public List<String> allRaw(DataviewFilter filter) {

        return jooq.select(DataConstants.dataview_details).from(schema.dataviews())
                .where(filter.toCondition())
                .fetch(schema.rawmapper());
    }

    public Dataview get(String id) {

        return lookup(id).orElseThrow(() -> new NoSuchEntityException(format("unknown dataview %s", id)));

    }

    public Optional<Dataview> lookup(String id) {

        return jooq.select().from(schema.dataviews())
                .where(DataConstants.dataview_id.equalIgnoreCase(id))
                .fetchOptional(schema.mapper());

    }

    public void add(Dataview dataview) {

        try {

            add(dataview, false);

        } catch (RuntimeException dae) {

            map(dae, on(duplicate)
                    .to(() -> new IllegalStateException(format("Dataview %s already exists.", dataview.id()))));
        }

    }

    // combines count and create/modified timestamp to capture a change 
    // in the data of a tenant, or globally if no tenant is in scope.
    // note: timestamp alone wouldn't capture removal of older entries.
    public String changestamp() {

        return jooq.select(DSL.count(), DSL.max(DataConstants.dataview_timestamp))
                .from(schema.dataviews())
                .fetchOne(r -> format("%s-%s", r.get(0), r.get(1)));

    }

    public void add(Dataview dataview, boolean ignoreOnDuplicate) {

        InsertOnDuplicateStep<?> insert = jooq.insertInto(schema.dataviews())
                .set(DataConstants.dataview_details, JSONB.valueOf(schema.binder().bind(dataview)));

        if (ignoreOnDuplicate)
            insert.onDuplicateKeyIgnore();

        insert.execute();

        
    }

    public void update(Dataview dataview) {

        try {

            jooq.update(schema.dataviews())
                    .set(DataConstants.dataview_details, JSONB.valueOf(schema.binder().bind(dataview)))
                    .where(DataConstants.dataview_id.eq(dataview.id()))
                    .execute();

        } catch (RuntimeException dae) {

            map(dae);
        }

    }

    public void remove(Dataview dataview) {

        jooq.delete(schema.dataviews()).where(DataConstants.dataview_id.equalIgnoreCase(dataview.id())).execute();

    }
}
