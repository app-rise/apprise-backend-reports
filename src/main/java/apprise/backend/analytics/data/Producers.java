package apprise.backend.analytics.data;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import com.fasterxml.jackson.databind.ObjectMapper;

import apprise.backend.analytics.Dataview;
import apprise.backend.ser.Binder;

@ApplicationScoped
public class Producers {
    

    @Produces
    Binder<Dataview> binder(ObjectMapper mapper) {

        return new Binder<>(mapper, Dataview.class);
    }
}
