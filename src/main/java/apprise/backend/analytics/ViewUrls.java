package apprise.backend.analytics;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class ViewUrls {

   public static enum Format { xlsx, csv}

   String embedUrl;
   
   Map<Format,String> downloadUrls = new HashMap<>();

}