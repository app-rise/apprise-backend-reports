package apprise.backend.analytics;

import static java.lang.String.format;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import apprise.backend.config.Banner;
import apprise.backend.config.ConfigView;

@ApplicationScoped
public class Config implements ConfigView {

    static final String analyticsKey = "analytics.key";
    static final String anlyticsTokenKey = "analytics.tokenKey";
    static final String analyticsUrl = "analytics.url";

    static final String defaultAnalyticsUrl = "http://bi";

    @Inject
    org.eclipse.microprofile.config.Config base;

    public String analyticsUrl() {
        return base.getOptionalValue(analyticsUrl, String.class).orElse(defaultAnalyticsUrl);
    }

    public String analyticsKey() {
        return base.getOptionalValue(analyticsKey, String.class).orElse(null);
    }

    public String analyticsTokenKey() {
        return base.getOptionalValue(anlyticsTokenKey, String.class).orElse(null);
    }


    @Override
    public void appendTo(Banner banner) {

        banner.append(analyticsUrl, analyticsUrl())
                .append(analyticsKey, analyticsKey() == null ? null : format("%s...", analyticsKey().substring(0, 3)))
                .append(anlyticsTokenKey, analyticsTokenKey() == null ? null : format("%s...", analyticsTokenKey().substring(0, 3)));
    }

   // @Override  // introduces later
    // public List<String> validate() {

    //     return CheckDsl.given(this)
    //             .check($ -> analyticsKey() != null).or(format("missing %s", analyticsKey))
    //             .andCollect();

    // }
}