package apprise.backend.analytics.metabase;

import static apprise.backend.analytics.metabase.Constants.embedDashboard;
import static apprise.backend.analytics.metabase.Constants.embedQuestion;
import static apprise.backend.analytics.metabase.Constants.embeddableDashboards;
import static apprise.backend.analytics.metabase.Constants.embeddableQuestions;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.UriBuilder;

import apprise.backend.analytics.ViewDescriptor;
import apprise.backend.analytics.ViewUrls;
import apprise.backend.analytics.ViewDescriptor.Type;
import apprise.backend.analytics.api.Dtos.ViewParams;
import apprise.backend.analytics.data.ViewService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ApplicationScoped
public class MetabaseService implements ViewService {

    @Inject
    TokenGenerator generator;

    @Inject
    MetabaseClient client;

    GenericType<List<ViewDescriptor>> viewDescriptorColl = new GenericType<>() {
    };

    @Override
    public List<ViewDescriptor> all() {

        var fetchDashboards = CompletableFuture.runAsync(() -> log.info("fetching dashkboards..."))
                .thenApplyAsync($ -> client.at(embeddableDashboards).get(viewDescriptorColl))
                .thenApply(ds -> ds.stream().map(d -> d.type(Type.dashboard)).collect(toList()));

        var fetchCards = CompletableFuture.runAsync(() -> log.info("fetching qustions..."))
                .thenApplyAsync($ -> client.at(embeddableQuestions).get(viewDescriptorColl))
                .thenApply(ds -> ds.stream().map(d -> d.type(Type.question)).collect(toList()));

        return fetchDashboards.thenCombineAsync(fetchCards, (first, second) -> {

            first.addAll(second);

            return first;

        }).join();

    }

    @Override
    @SneakyThrows
    public ViewUrls urlsFor(ViewDescriptor descriptor, ViewParams params) {

        var dto = descriptor.type() == Type.dashboard ? new Dtos.Token().dashboard(descriptor.id())
                : new Dtos.Token().question(descriptor.id());

        var token = generator.generate(dto.params(params.encoded()));

        var route = descriptor.type() == Type.dashboard ? embedDashboard : embedQuestion;

        var routeWithToken = format(route, token);

        Function<String, String> appendParams = (url) -> {

            var builder = UriBuilder.fromPath(url);

            var paramsAndDefaults = new HashMap<>(params.plain());

            var full = paramsAndDefaults.entrySet().stream().reduce(builder,
                    (b, e) -> b.queryParam(e.getKey().replaceAll("\\s+", "_"), e.getValue()), (u1, u2) -> u2);

            return full.toString();
        };

        var embedUrl = appendParams.apply(routeWithToken);

        var urls = new ViewUrls().embedUrl(embedUrl);

        if (descriptor.type() == Type.question) {

            var downloadUrls = Stream.of(ViewUrls.Format.values()).collect(toMap(

                    f -> f,

                    f -> appendParams.apply(format("%s.%s", routeWithToken, f)))

            );

            urls.downloadUrls(downloadUrls);
        }

        return urls;

    }

}
