package apprise.backend.analytics.metabase;

import static apprise.backend.analytics.metabase.Constants.apikey_header;
import static java.lang.String.format;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.ws.rs.client.Invocation.Builder;

import apprise.backend.analytics.Config;
import apprise.backend.api.client.ServiceCaller;

@Dependent
public class MetabaseClient extends ServiceCaller {

    @Inject
    Config config;
    

    // delegates to generic service, but prepends Metabase api prefix and sets the header to the api key.
    @Override
    public Builder at(String path, String... params) {
        return super.at(format("%s/%s",config.analyticsUrl(),path), params).header(apikey_header, config.analyticsKey());
    }
}
