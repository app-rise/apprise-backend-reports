package apprise.backend.analytics.metabase;

public class Constants {
    

    public static final String apikey_header = "x-api-key";
    public static final String embeddableDashboards = "api/dashboard/embeddable";
    public static final String embeddableQuestions = "api/card/embeddable";
    public static final String embedDashboard = "bi/embed/dashboard/%s";
    public static final String embedQuestion = "bi/embed/question/%s";
}
