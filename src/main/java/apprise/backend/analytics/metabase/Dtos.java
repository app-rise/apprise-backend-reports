package apprise.backend.analytics.metabase;

import java.util.Map;

import lombok.Data;

public class Dtos {

    @Data
    public static class Token {

        Resource resource;

        public Token dashboard(String id) {
            resource = new Resource().dashboard((Long.valueOf(id)));
            return this;
        }

        public Token question(String id) {
            resource = new Resource().question((Long.valueOf(id)));
            return this;
        }

        Map<String,String> params = Map.of();

        
        @Data
        static class Resource {

            Long dashboard;
            Long question;

        }
    }


}
