package apprise.backend.analytics.metabase;

import static io.jsonwebtoken.SignatureAlgorithm.HS256;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.fasterxml.jackson.databind.ObjectMapper;

import apprise.backend.analytics.Config;
import io.jsonwebtoken.Jwts;
import lombok.SneakyThrows;

@Dependent
public class TokenGenerator {

    @Inject
    ObjectMapper mapper;

    @Inject
    Config config;

    @SneakyThrows
    public String generate(Dtos.Token token) {

        var serialised = mapper.writeValueAsString(token);

        return Jwts.builder().setHeaderParam("typ", "JWT").setPayload(serialised).signWith(HS256, config.analyticsTokenKey().getBytes()).compact();

    }

}
