package apprise.backend.analytics;

import static apprise.backend.analytics.Dataview.DataviewState.active;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import apprise.backend.model.Lifecycle;
import apprise.backend.model.Lifecycled;
import apprise.backend.validation.CheckDsl;
import apprise.backend.validation.Mode;
import apprise.backend.validation.Validated;
import lombok.Data;

@Data
public class Dataview implements Lifecycled<Dataview.DataviewState,Dataview>, Validated {
    

    public static enum DataviewState { active, inactive }

    String id;

    List<ViewDescriptor> views;

    Lifecycle<DataviewState> lifecycle = new Lifecycle<>(active);

    @JsonAnyGetter
    @JsonAnySetter
    Map<String,Object> details = new HashMap<>();


    public void updateWith(Dataview other) {

        details(other.details()).state(other.state()).lifecycle(other.lifecycle()).views(other.views());
    }

    @Override
    public List<String> validate(Mode mode) {

        return CheckDsl.given(this)
                .check($ -> id != null).or("missing identifier")
                .check($ -> views != null && !views.isEmpty()).or("missing views")
                .andCollect();
    }
}
